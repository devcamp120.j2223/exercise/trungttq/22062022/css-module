import avatar from "./assets/images/48.jpg";

import style from "./App.module.css";

function App() {
  return (
    <div className={style.dcContainer}>
      <div>
        <img src={avatar} alt="avatar" className={style.dcAvatar}/>
      </div>
      <div className={style.dcQuote}>
        This is one of the best developer blogs on the planet! I read it daily to improve my skills.
      </div>
      <div>
        <span className={style.dcName}>
          Tammy stevens
        </span>
        <span className={style.dcJob}>
          &nbsp; * &nbsp;Front End Developer
        </span>
      </div>
    </div>
  );
}

export default App;
